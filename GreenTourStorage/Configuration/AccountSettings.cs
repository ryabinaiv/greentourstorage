﻿using System.Collections.Generic;
using GreenTourStorage.Models;

namespace GreenTourStorage.Configuration
{
    public class AccountSettings
    {
         public List<Person> ExistPersons { get; set; }
    }
}
