﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GreenTourStorage.Dtos;
using GreenTourStorage.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GreenTourStorage.Controllers.ApiControllers
{
    [ApiController]
    [Route("api/v1/Tenant")]
  //  [Authorize]
    public class TenantApiController
    {
        private readonly IPaymaster _paymaster;
        private readonly IMapper _mapper;

        public TenantApiController(IPaymaster paymaster, IMapper mapper)
        {
            _paymaster = paymaster;
            _mapper = mapper;
        }

        [HttpGet]
        public IList<TenantDto> GetAll()
        {
            return _paymaster.GetAllTenants()
                    .Select(q=> _mapper.Map<TenantDto>(q))
                    .ToList();
        }

        [HttpGet("{id:int}")]
        public TenantDto GetTenant(int id)
        {
            return _mapper.Map<TenantDto>(_paymaster.GetTenant(id));
        }

        [HttpPost]
        public TenantDto AddTenant(TenantCreateDto tenantDto )
        {
            var tenant = _paymaster.AddTenant(tenantDto);
            return _mapper.Map<TenantDto>(tenant);
        }

        [HttpPut("{id:int}")]
        public TenantDto Update([FromRoute] int id, TenantDto dto)
        {
            var tenant = _paymaster.Update(dto);
            return _mapper.Map<TenantDto>(tenant);
        }

        [HttpPut("{id:int}/BanTenant")]
        public TenantDto BanTenant([FromRoute] int id )
        {
            var tenant = _paymaster.BanTenant(id);
            return _mapper.Map<TenantDto>(tenant);
        }

        [HttpPut("{id:int}/KickOutTenant")]
        public TenantDto KickOutTenant([FromRoute] int id )
        {
            var tenant = _paymaster.KickOutTenant(id);
            return _mapper.Map<TenantDto>(tenant);
        }

        [HttpPut("{id:int}/TakeInTenant")]
        public TenantDto TakeInTenant([FromRoute] int id )
        {
            var tenant = _paymaster.TakeInTenant(id);
            return _mapper.Map<TenantDto>(tenant);
        }
    }
}
