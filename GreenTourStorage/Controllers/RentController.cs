﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GreenTourStorage.Models;
using GreenTourStorage.Queries;
using GreenTourStorage.Repository;
using GreenTourStorage.Services;
using Microsoft.AspNetCore.Authorization;

namespace GreenTourStorage.Controllers
{
    [Authorize]
    public class RentController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly IStorekeeper _storekeeper;
        private readonly IRentQueries _rentQueries;

        private class ArticleCompare : IComparer<Article>
        {
            public int Compare(Article x, Article y)
                => x.CompareTo(y);
        }

        public RentController(IStorekeeper storekeeper, IArticleService articleService, IRentQueries rentQueries)
        {
            _storekeeper = storekeeper;
            _articleService = articleService;
            _rentQueries = rentQueries;
        }

        [HttpGet]
        [Route("Rent/{id:int}")]
        public async Task<IActionResult> Rent(int id)
        {
            return View(await _rentQueries.GetRent(id));
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _rentQueries.GetRents());
        }

        [HttpGet]
        public IActionResult Create()
        {
            var compare = new ArticleCompare();
            var particularArticles = _articleService.GetParticularOnStorage();
            particularArticles.Sort(compare);
            var multipleArticles = _articleService.GetMultipleOnStorage();
            multipleArticles.Sort(compare);

            var createRentDto = new CreateRentDto
            {
                Items = particularArticles.Select(q =>
                    new CreateRentItemDto
                    {
                        ArticleId = q.RegNum,
                        Type = q.Type,
                        Category = q.Category,
                        ActualCount =  1,
                        Description = q.Description,
                        Name = q.Name,
                        Price = q.Price.ToString(CultureInfo.InvariantCulture),
                        RequestedCount = 0,
                    }) .Concat(
                        multipleArticles.Select(q=>
                            new CreateRentItemDto
                            {
                                ArticleId = q.RegNum,
                                Type = q.Type,
                                Category = q.Category,
                                ActualCount = q.NowCount,
                                Description = q.Description,
                                Name = q.Name,
                                Price = q.Price.ToString(CultureInfo.InvariantCulture),
                                RequestedCount = 0,
                            }
                    ))
                    .ToArray()
            };
            return View(createRentDto);
        }




        [HttpPost]
        public async Task<IActionResult> Create (CreateRentDto dto)
        {
            var rent = await _storekeeper.CreateRent(dto);
            return RedirectToAction(rent.RentId.ToString());
        }

        [HttpPost]
        [Route("Rent/{id:int}_Finish")]
        [Route("Rent/{id:int}/Finish")]
        public IActionResult Finish (FinishRentDto dto)
        {
            _storekeeper.FinishRent(dto).Wait();
            return RedirectToAction(dto.RentId.ToString());
        }
    }

    public class CreateRentItemDto
    {
        public int ArticleId { get; set; }
        public ArticleType Type { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ActualCount { get; set; }
        public int RequestedCount { get; set; }
        public string Price { get; set; }
        public bool Joined { get; set; }
    }

    public class CreateRentDto
    {
        public CreateRentItemDto[] Items { get; set; }
        public string User { get; set; }
    }


    public class FinishRentItemDto
    {
        public int ArticleId { get; set; }

        public int ToRepair { get; set; }
        public int Returned { get; set; }
        public string Comment { get; set; }

    }

    public class FinishRentDto
    {
        public int RentId { get; set; }
        public FinishRentItemDto[] Items { get; set; }
    }
}
