﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using GreenTourStorage.Services;
using GreenTourStorage.Models;
using GreenTourStorage.Queries;
using Microsoft.AspNetCore.Authorization;

namespace GreenTourStorage.Controllers
{
    [Authorize]
    public class StorageController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly IStorekeeper _storekeeper;
        private readonly IArticleQueries _articleQueries;

        private class CategoriesCompare : IComparer<Category>
        {
            public int Compare(Category x, Category y)
            {
                return x?.Name switch
                {
                    null when y?.Name == null => 0,
                    null => -1,
                    _ => y?.Name == null ? 1 : string.Compare(x.Name, y.Name, StringComparison.Ordinal)
                };
            }
        }

        private class ArticleCompare : IComparer<Article>
        {
            public int Compare(Article x, Article y)
                => x.CompareTo(y);
        }

        public StorageController (IArticleService articleService, IStorekeeper storekeeper, IArticleQueries articleQueries)
        {
            _articleService = articleService;
            _storekeeper = storekeeper;
            _articleQueries = articleQueries;
        }

        public IActionResult Index()
        {
            var compare = new ArticleCompare();
            var particularArticles = _articleService.GetAllParticular();
            particularArticles.Sort(compare);
            var multipleArticles = _articleService.GetAllMultiple();
            multipleArticles.Sort(compare);
            return View((particularArticles, multipleArticles));
        }


        [HttpGet]
        public IActionResult AddArticle()
        {
          var categories =  _articleService.GetAllCategories();
            categories.Sort(new CategoriesCompare());
            return View(categories);
        }


        [HttpPost]
        public IActionResult AddArticle (string articleName, string category,
            double price, string type,
                                        double? weight, int? maCount,
                                        int? regNum = null,
                                        string description = null,
                                        string comment = null)
        {
            try
            {
                if (type == "particular")
                {
                    var resultReg = _articleService.RegisterParticularArticle
                        (articleName, category, price, description, weight, regNum, comment!=null, comment);
                    return RedirectToAction("ParticularArticle", new { reg = resultReg });
                }

                if (type == "multiple")
                {
                    var resultReg = _articleService.RegisterMultipleArticle
                        (articleName, category, price, maCount.Value, description, weight);
                    return RedirectToAction("MultipleArticle", new { reg = resultReg });
                }

            }
            catch (Exception e)
            {
            //    throw new NotImplementedException();
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult AddCategories()
        {
            var categories = _articleService.GetAllCategories();
            categories.Sort(new CategoriesCompare());
            return View(categories);
        }


        [HttpPost]
        public IActionResult AddCategories(string category)
        {
            try
            {
                _articleService.CreateCategory(category);
            }
            catch (Exception e) // TODO!!
            {
                throw new NotImplementedException();
            }
            return RedirectToAction("AddCategories");
        }

        [Route("Storage/ParticularArticle/{reg:int}")]
        [HttpGet]
        public IActionResult ParticularArticle(int reg)
        {
            return View((_articleService.GetParticularArticle(reg),
                _storekeeper.GetArticleRentStory(reg), _articleService.GetAllCategories()));
        }

        [Route("Storage/ParticularArticle/{reg:int}")]
        [HttpPost]
        public IActionResult ParticularArticle(int reg, string articleName,
                                                decimal? weight, double price, string category,
                                        string description = null, string comment = null)
        {
            _articleService.EditArticle(reg, articleName, price, description, category);
            var bm = comment != null;
            _articleService.SetBlackMarkParticularArticle(reg, bm, comment);
            if (weight.HasValue)
                _articleService.SetWeightParticularArticle(reg, weight.Value);
            return RedirectToAction("ParticularArticle", new { reg = reg });
        }

        [Route("Storage/MultipleArticle/{reg:int}")]
        [HttpGet]
        public async Task<IActionResult> MultipleArticle(int reg)
        {
            var ma = await _articleQueries.GetArticle(reg);
            return View(( ma,  _articleService.GetAllCategories()));
        }

        [Route("Storage/MultipleArticle/{reg:int}")]
        [HttpPost]
        public IActionResult MultipleArticle(int reg, string articleName,
                                            decimal? weight, double price, string category,
                                            int? maAdditionalCount,
                                            string description = null)
        {
            _articleService.EditArticle(reg, articleName, price, description, category);
            if ((maAdditionalCount.HasValue) && (maAdditionalCount.Value > 0))
                _articleService.RegisterAdditiveMultipleArticle(reg, maAdditionalCount.Value);
            if (weight.HasValue)
                _articleService.SetWeightMultipleArticle(reg, weight.Value);
            return RedirectToAction("MultipleArticle", new { reg = reg });
        }
    }
}
