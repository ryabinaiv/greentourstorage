﻿using System.Linq;
using GreenTourStorage.Dtos;
using Microsoft.AspNetCore.Mvc;
using GreenTourStorage.Services;
using Microsoft.AspNetCore.Authorization;

namespace GreenTourStorage.Controllers
{
    [Authorize]
    public class TenantController: Controller
    {
        private static IPaymaster _paymaster;

        public TenantController(IPaymaster paymaster)
        {
            _paymaster = paymaster;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_paymaster.GetAllTenants().ToList());
        }

        [HttpPost]
        public IActionResult Index(string phone)
        {
          //  _paymaster.AddClubman(phone);
            return RedirectToAction("Index");
        }

        [HttpGet("Tenant/Add")]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost("Tenant/Add")]
        public IActionResult Add(TenantCreateDto createDto)
        {
            var tenant = _paymaster.AddTenant(createDto);
            return RedirectToAction("Index");
        }

        [Route("Tenant/{reg:int}")]
        [HttpGet]
        public IActionResult Tenant(int reg)
        {
            var tenant = _paymaster.GetTenant(reg);
            return View(tenant);
        }



    }
}
