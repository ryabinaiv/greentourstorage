﻿namespace GreenTourStorage.Dtos
{
    public class TenantDto
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string NickName { get; set; }
        public string State { get; set; }
    }

    public class TenantCreateDto
    {
        public string Phone { get; set; }
        public string NickName { get; set; }
        public bool IsClubman { get; set; }
    }
}
