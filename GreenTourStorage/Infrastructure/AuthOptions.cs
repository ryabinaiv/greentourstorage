﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace GreenTourStorage.Infrastructure
{
    public class AuthOptions
    {
        public const string ISSUER = "GTServer"; // издатель токена
        public const string AUDIENCE = "GTAuthClient"; // потребитель токена
        const string KEY = "kdnGV$dyG@i6~1AB}FiwYDw#NZrSLy";   // ключ для шифрации
        public const int LIFETIME = 1; // время жизни токена - 1 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
