﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GreenTourStorage.Models;

namespace GreenTourStorage.Infrastructure
{
    public class StorageContext : DbContext
    {
        public DbSet<ParticularArticle> ParticularArticles { get; set; }
        public DbSet<MultipleArticle> MultipleArticles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Rent> Rents { get; set; }
        public DbSet<RentItem> RentItems { get; set; }
        public DbSet<Tenant> Tenants { get; set; }

        public StorageContext (DbContextOptions<StorageContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasKey(u => u.CategoryId);
            modelBuilder.Entity<MultipleArticle>().HasKey(u => u.Id);
            modelBuilder.Entity<MultipleArticle>().Ignore(q => q.Type);

            modelBuilder.Entity<ParticularArticle>().HasKey(u => u.Id);
            modelBuilder.Entity<ParticularArticle>().Ignore(q => q.Type);

            modelBuilder.Entity<Tenant>().HasKey(u => u.Id);

            modelBuilder.Entity<Rent>().HasKey(u => u.RentId);
            modelBuilder.Entity<Rent>().Ignore(q => q.FinalCost);
            modelBuilder.Entity<RentItem>().HasKey(u => new {u.RentId, u.ArticlesId});
        }
    }
}
