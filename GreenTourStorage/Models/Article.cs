﻿using System;
using GreenTourStorage.Repository;

namespace GreenTourStorage.Models
{
    public abstract class Article
    {
        public Article()
        {

        }
        public Article (int regNum, string name, string category, double price, ArticleType type,
            string desc = "", double? weight = null)
        {
            if (string.IsNullOrEmpty(name) || price <= 0 || category == "" || category == null || regNum < 0)
            {
                throw new ArgumentOutOfRangeException("Ошибка заполнения полей");
            }

            if (weight.HasValue && weight.Value <= 0)
            {
                throw new ArgumentOutOfRangeException("Ошибка заполнения полей");
            }

            RegNum = regNum;
            Name = name;
            Category = category;
            Price = (decimal)price;
            Description = desc;
            Weight = (decimal?)weight;
            Type = type;
        }
        public int Id { get; set; }
        public int RegNum { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }

        public virtual ArticleType Type { get;}
        public string Description { get; set; }
        public decimal? Weight { get; set; }

        // TODO :: Photo
        public int CompareTo (Article y)
        {
            switch (String.Compare(Category, y.Category, StringComparison.Ordinal))
            {
                case 1:
                    return 1;
                case -1:
                    return -1;
                default:
                    return RegNum.CompareTo(y.RegNum);
            }
        }

        public abstract void ReturnToStorage(RentItem item);

        public abstract void GoToRent((int rent , int count ) rentInfo);

        public abstract bool IsReadyToRent();
    }
}
