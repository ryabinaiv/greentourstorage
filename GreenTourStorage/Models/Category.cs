﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenTourStorage.Models
{
    public class Category
    {
        public Category()
        {
        }

        public Category (string name)
        {
            if ((name == "") || (name == null))
                throw new ArgumentOutOfRangeException("Ошибка заполнения полей");
            Name = name;
        }
       
        public int CategoryId { get; set; }
        public string Name { get; set;}
    }
}
