﻿using System;
using System.Collections.Generic;
using System.Linq;
using GreenTourStorage.Repository;

namespace GreenTourStorage.Models
{
    /// <summary>
    /// Класс множественных вещей на складе, которые не имеют уникальноти и равны между собой.
    /// Для разбиения на группы используются MultipleSet'ы
    /// </summary>
    public class MultipleArticle : Article
    {

        private int _actualId;
        //private readonly List<MultipleArticleSet> _multipleArticleSetsList;

        public MultipleArticle() {}

        public MultipleArticle(int regNum, string name, string category, double price, int totalCount, string desc = null,
                               double? weight = null, int? nowCount = null, List<MultipleArticleSet> list = null)
                                : base(regNum, name, category, price, ArticleType.Multiple, desc, weight)
        {
            if (totalCount <= 0 || nowCount.HasValue && nowCount.Value < 0)
            {
                throw new ArgumentOutOfRangeException("Ошибка заполнения полей");
            }

            TotalCount = totalCount;
            NowCount = nowCount ?? totalCount;
            // _multipleArticleSetsList = new List<MultipleArticleSet>();

            if (list == null)
            {
                _actualId = 0;
            }
            else
            {
                // _multipleArticleSetsList = list;
                // _actualId = _multipleArticleSetsList.Last().Id;
                _actualId++;
            }
        }

        public override ArticleType Type => ArticleType.Multiple;
        public int TotalCount { get; private set; }
        public int NowCount { get; private set; }
        public int InRepairCount { get; private set; }

        // public List<MultipleArticleSet> GetSetsList() => _multipleArticleSetsList;

        public int GenericSetId() => _actualId++;

        public void GoToRent(int count)
        {

        }

        public void ReturnToStorage(int count)
        {

            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            // var rentSet = new MultipleArticleSet(GenericSetId(), this, count);
            // _multipleArticleSetsList.Add(rentSet);
            NowCount += count;
        }

        public void ReturnWithBlackMark(int setid, string comment, int? count = null)
        {
            // var set = _multipleArticleSetsList.Find(x => x.Id == setid);
            // if (set == null)
            // {
            //     throw new NullReferenceException();
            // }

            // NowCount += set.AddBlackMark(comment, count ?? set.CountInSet);
        }

        public void ReturnMultipleSet(int setid)
        {
            // var set = _multipleArticleSetsList.Find(x => x.Id == setid);
            // if (set == null)
            // {
            //     throw new NullReferenceException();
            // }
            //
            // if (set.BlackMark)
            // {
            //     throw new ArgumentOutOfRangeException("Снаряжение требует ремонта, его нельзя вернуть в общий доступ");
            // }
            //
            // NowCount += set.CountInSet;
            // _multipleArticleSetsList.Remove(set);
        }

        public void WriteOfSet(int setid)
        {
            // var set = _multipleArticleSetsList.Find(x => x.Id == setid);
            // if (set == null)
            //     throw new NullReferenceException();
            // TotalCount -= set.CountInSet;
            // set.WriteOf();
        }

        public void FixSet(int setid)
        {
            // var set = _multipleArticleSetsList.Find(x => x.Id == setid);
            // if ((set == null) || (set.IsWriteOf))
            //     throw new NullReferenceException();
            // set.CancelBlackMark();
            // _multipleArticleSetsList.Remove(set);
            // NowCount += set.CountInSet;
        }

        public void IncreaseArticleCount(int addcount)
        {
            TotalCount += addcount;
            NowCount += addcount;
        }


        public void ToRepair(in int itemCount)
        {
            NowCount -= itemCount;
            InRepairCount += itemCount;
        }

        public void FixIt(in int itemCount)
        {
            NowCount += itemCount;
            InRepairCount -= itemCount;
        }

        public override void ReturnToStorage(RentItem item)
        {
            ReturnToStorage(item.Count);
            if (item.ToRepair != 0)
            {
                ToRepair(item.Count);
            }
        }

        public override void GoToRent((int rent, int count) rentInfo)
        {
            if (rentInfo.count <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (NowCount < rentInfo.count)
            {
                throw new ArgumentOutOfRangeException("Недостаточное количество снаряжения на складе");
            }

            // var rentSet = new MultipleArticleSet(GenericSetId(), this, count);
            // _multipleArticleSetsList.Add(rentSet);
            NowCount -= rentInfo.count;
        }

        public override bool IsReadyToRent()
        {
            return NowCount > 0;
        }
    }
}
