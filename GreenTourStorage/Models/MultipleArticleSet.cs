﻿using System;

namespace GreenTourStorage.Models
{
    /// <summary>
    /// BlackMark - mark means that this set needs in fix
    /// IsWriteOf - mark means that this set is write of (for article rent history) // is it good and fits "one duty"
    /// </summary>
    public class MultipleArticleSet
    {
        public MultipleArticleSet (int id, MultipleArticle article, int count)
        {
            if (id < 0 || article == null || count <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            Id = id;
            Article = article;
            CountInSet = count;
            BlackMark = false;
            Comment = null;
            IsWriteOf = false;
        }

        public int Id { get; }
        //public int ArticleId { get; }
        //public int ArticleRegNum { get; }
        //public double Price { get; }
        public MultipleArticle Article { get; }
        public int CountInSet { get; private set; }
        public bool BlackMark { get; private set; }
        public string Comment { get; private set; }
        public bool IsWriteOf { get; private set; }

        public int AddBlackMark(string comment, int count)
        {
            BlackMark = true;
            Comment = comment;
            CountInSet = count;
            return CountInSet - count;
        }

        public void CancelBlackMark()
        {
            BlackMark = false;
            Comment = null;
        }

        public void WriteOf() => IsWriteOf = true;
    }
}
