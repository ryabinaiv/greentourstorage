﻿using System;
using GreenTourStorage.Repository;

namespace GreenTourStorage.Models
{
    /// <summary>
    /// Класс единственной, уникальной вещи на складе.
    /// Черная метка - необходимость в ремонте, мытье. Комментарий идет в паре и отсутствует при отсутствии метки.
    /// </summary>
    public class ParticularArticle : Article
    {
        public ParticularArticle()
        {
        }

        public ParticularArticle(int regNum, string name, string category,
                        double price, string desc = null,
                                double? weight = null, string comment = null, bool bm = false)
                                    : base(regNum, name, category, price, ArticleType.Particular, desc, weight)
        {
            BlackMark = bm;
            Comment = comment;
            RentId = null;
            IsWriteOf = false;
        }
        public bool BlackMark { get; private set; }
        public string Comment { get; private set; }
        public int? RentId { get; private set; }
        public bool IsWriteOf { get; private set; }
        public override ArticleType Type  => ArticleType.Particular;
        public override void ReturnToStorage(RentItem item)
        {
            ReturnToStorage();
            if (item.ToRepair != 0)
            {
                //Todo
               AddBlackMark(item.Comment);
            }
        }

        public override void GoToRent((int rent, int count) rentInfo)
        {
            if (!IsReadyToRent())
            {
                throw new ArgumentException($"{RegNum} не доступен для аренды");
            }

            RentId = rentInfo.rent;
        }

        public override bool IsReadyToRent()
        {
            return !IsWriteOf && !BlackMark && !InRent;
        }

        public bool InRent => RentId != null;

        public void WriteOf() => IsWriteOf = true;

        public void AddBlackMark(string comment)
        {
            BlackMark = true;
            Comment = comment;
        }

        public void ReturnToStorage() => RentId = null;

        public void CancelBlackMark()
        {
            BlackMark = false;
            Comment = null;
        }

        public void FixIt() => CancelBlackMark();
    }


}
