﻿using System;
using System.Collections.Generic;
using System.Linq;
using GreenTourStorage.Controllers;
using GreenTourStorage.Repository;

namespace GreenTourStorage.Models
{
    /// <summary>
    /// Класс аренды. При создании вычисляется стоимость аренды на неделю.
    /// Есть метод внесения первоначальной суммы.
    /// При завершении - выводится значение необходимой доплаты за задержку(если она была)
    /// Дя списка вещей, взятых в аренду используется RentSet'ы.
    /// </summary>
    public class Rent
    {
        public Rent() {}

        public Rent(int rentId, Tenant tenant, string comment)
        {
            if (rentId < 0)
            {
                throw new NullReferenceException();
            }

            RentId = rentId;
            TenantId = tenant.Id;
            IsFreeRent = tenant.IsClubman;

            RentItems = new List<RentItem>();
            Comment = comment;
            MadePayValue = 0;
            RentBeginDate = DateTime.Today;
        }

        public void MakePay(decimal payValue) => MadePayValue += payValue;

        public decimal Weeks() => Math.Floor(((RentEndDate ?? DateTime.Today) - RentBeginDate).Days / (decimal) 7) + 1;

        public void FinishRent(FinishRentDto dto)
        {
            RentEndDate = DateTime.Today;
            IsFinished = true;
            var returnItems =
                dto.Items.ToDictionary(q => q.ArticleId, q=>q);

            foreach (var rentItem in RentItems)
            {
                rentItem.ApplyReturn(returnItems[rentItem.ArticlesId]);
            }
        }

        public const decimal Percent = (decimal) 0.1;
        public int RentId { get; private set; }
        public int TenantId { get;private set; }
        public string Comment { get; private set;}
        public bool IsFreeRent { get; private set;}
        public decimal FinalCost =>  RentCostInWeek * Weeks();
        public bool IsFinished { get; private set;}

        public decimal RentCostInWeek { get; private set;}


        public decimal MadePayValue { get; private set; }
        public DateTime RentBeginDate { get; private set; }
        public DateTime? RentEndDate { get; private set; }

        public IList<RentItem> RentItems { get; private set; }

        public void AddArticleInRent(Article article, int count)
        {

            RentItems.Add(
                new RentItem(RentId, article.RegNum, article.Type, article.Price, count));

            if (!IsFreeRent)
            {
                RentCostInWeek = RentItems.Sum(item => item.Price * Percent * item.Count);
            }

        }
    }

    public class RentItem
    {
        public RentItem()
        {
        }

        public RentItem(int rentId, int articlesId, ArticleType type, decimal price, int count)
        {
            RentId = rentId;
            ArticlesId = articlesId;
            ArticleType = type;
            Price = price;
            Count = count;
        }

        //public Rent Rent { get; set; }
        public int RentId { get; private set; }
        public int ArticlesId { get;  private set;}
        // public ArticleType ArticleType { get; private set; }
        // public string ArticleType { get; private set; }
        public decimal Price { get; private set; }
        public int Count { get; private set; }
        public int ToRepair { get; private set; }
        public int Returned { get; private set; }
        public ArticleType ArticleType { get; set; }

        public string Comment { get; private set; }

        public void ApplyReturn(FinishRentItemDto returnItem)
        {
            Returned = returnItem.Returned;
            ToRepair = returnItem.ToRepair;
            Comment = returnItem.Comment;
        }
    }
}

