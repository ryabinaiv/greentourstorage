﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreenTourStorage.Models
{

    /// <summary>
    /// ParticularArticlesList - rent list of particular articles
    /// MultipleArticleSetsList - rent list of multiple article sets
    /// </summary>
    public class RentSet
    {
        private List<ParticularArticle> ParticularArticlesList;
        private List<MultipleArticleSet> MultipleArticleSetsList;

        public RentSet()
        {
            ParticularArticlesList = new List<ParticularArticle>();
            MultipleArticleSetsList = new List<MultipleArticleSet>();
        }

        public void AddParticularArticle(ParticularArticle pa)
            => ParticularArticlesList.Add(pa);
        public void AddMultipleArticleSet(MultipleArticleSet mas)
            => MultipleArticleSetsList.Add(mas);
    
        public List<ParticularArticle> GetParticularArticlesList()
            => ParticularArticlesList;
        public List<MultipleArticleSet> GetMultipleArticleSetsList()
            => MultipleArticleSetsList;
    }
}
