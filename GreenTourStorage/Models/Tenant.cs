﻿namespace GreenTourStorage.Models
{

    public enum TenantType
    {
        Simple,
        Banned,
        Clubman,
    }


    public class Tenant
    {
        public static string phonePattern = @"8-[0-9]{3}-[0-9]{3}-[0-9]{4}";

        public Tenant()
        {

        }

        public Tenant(string phone, string nickname, bool isClubman = false)
        {
            // if (!Regex.IsMatch(phone, phonePattern, RegexOptions.IgnoreCase))
            //     throw new ArgumentOutOfRangeException("Не соблюден формат телефонного номера");
            Phone = phone;
            Nickname = nickname;
            State = isClubman ? TenantType.Clubman : TenantType.Simple;
        }

        public int Id { get; private set; }
        public string Phone { get;  set; }
        public string Nickname { get; set; }

        public TenantType State { get; private set; }

        public bool IsClubman => State == TenantType.Clubman;
        public bool IsBanned => State == TenantType.Banned;

        public void BanTenant() => State = TenantType.Banned;
        public void TakeInClub() =>  State = TenantType.Clubman;
        public void TakeOffClub() => State = TenantType.Simple;

        public void ChangePhone(string tenantDtoPhone)
        {
            Phone = tenantDtoPhone;
        }
    }
}
