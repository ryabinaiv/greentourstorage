﻿using System.Linq;
using System.Threading.Tasks;
using GreenTourStorage.Infrastructure;
using GreenTourStorage.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GreenTourStorage.Queries
{
    public interface IArticleQueries
    {
        Task<MultipleArticleViewModel> GetArticle(int regNum);
    }

    public class ArticleQueries : IArticleQueries
    {
        private readonly StorageContext _db;

        public ArticleQueries(StorageContext db)
        {
            _db = db;
        }

        public async Task<MultipleArticleViewModel> GetArticle(int regNum)
        {

            var inRentTask = await _db.RentItems.Where(q => q.ArticlesId == regNum)
                .Join(_db.Rents.Where(q => !q.IsFinished), q => q.RentId, r => r.RentId,
                    (ri, r) =>
                        new MultipleArticleRentStoryItem
                        {
                            Count = ri.Count,
                            Tenant = r.TenantId.ToString(),
                            RentDateTime = r.RentBeginDate,
                            RentId = r.RentId
                        }
                ).ToArrayAsync();

            var item= await _db.MultipleArticles.FirstOrDefaultAsync(
                q => q.RegNum == regNum);

            return new MultipleArticleViewModel
            {
                Category = item.Category,
                Description = item.Description,
                Name = item.Name,
                Price = item.Price,
                Weight = item.Weight,
                NowCount = item.NowCount,
                RegNum = item.RegNum,
                TotalCount = item.TotalCount,
                InRepairCount = item.InRepairCount,
                InRent = inRentTask
            };
        }
    }
}
