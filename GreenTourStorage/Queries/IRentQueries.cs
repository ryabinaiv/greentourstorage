﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenTourStorage.Infrastructure;
using GreenTourStorage.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace GreenTourStorage.Queries
{
    public interface IRentQueries
    {
        Task<List<RentListItemViewModel>> GetRents();
        Task<RentViewModel> GetRent(int rentId);
    }

    public class RentQueries: IRentQueries
    {
        private readonly StorageContext _db;

        public RentQueries(StorageContext db)
        {
            _db = db;
        }


        public  async Task<List<RentListItemViewModel>> GetRents()
        {
            return await _db.Rents.Join(_db.Tenants, q => q.TenantId, t => t.Id,
                (r, t) => new RentListItemViewModel
                {
                    RenId = r.RentId,
                    Weeks = r.Weeks(),
                    PaidSum = r.MadePayValue,
                    RentDate = r.RentBeginDate,
                    ReturnDate = r.RentEndDate,
                    TetantId = r.TenantId,
                    TetantName = t.Phone,
                    IsFinished = r.IsFinished,
                }).ToListAsync();
        }

        public async Task<RentViewModel> GetRent(int rentId)
        {
            var rent= await _db.Rents.Where(q=>q.RentId == rentId)
                .Join(_db.Tenants, q => q.TenantId, t => t.Id,
                (r, t) => new RentViewModel
                {
                    RenId = r.RentId,
                    Weeks = r.Weeks(),
                    PaidSum = r.MadePayValue,
                    RentDate = r.RentBeginDate,
                    ReturnDate = r.RentEndDate,
                    TetantId = r.TenantId,
                    IsFinished = r.IsFinished,
                    TetantName = t.Phone,
                    FinalCost = r.RentCostInWeek * r.Weeks()
                }).FirstOrDefaultAsync();

            if (rent != null)
            {
                rent.Items = await _db.RentItems.Where(q => q.RentId == rentId)
                    .Join(
                        _db.MultipleArticles.Select(q => new {q.RegNum, q.Name})
                            .Concat(_db.ParticularArticles.Select(q => new {q.RegNum, q.Name})),
                        i => i.ArticlesId, p => p.RegNum,
                        (i, p) => new RentItemViewModel
                        {
                            ArticlesName = p.Name,
                            RentId = i.RentId,
                            Type = i.ArticleType,
                            Count = i.Count,
                            Price = i.Price,
                            ArticlesId = i.ArticlesId,
                            ToRepair = i.ToRepair,
                            Returned = i.Returned,
                        })
                    .ToListAsync();
            }
            return rent;
        }
    }
}
