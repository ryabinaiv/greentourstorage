﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenTourStorage.Models;
using GreenTourStorage.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace GreenTourStorage.Repository
{
    public enum ArticleType
    {
        Particular,
        Multiple
    }

    public interface IArticleRepository
    {
        int GenericId();
        int GenericId(int? regnum);
        ParticularArticle GetParticular(int id);
        MultipleArticle GetMultiple(int id); // all sets besides has mark WriteOf
        void InsertParticular(ParticularArticle pa);
        void InsertMultiple(MultipleArticle ma);
        Category GetCategory(string categoryName);
        void InsertCategory(Category category);
        List<Category> GetAllCategory();
        List<ParticularArticle> GetAllParticular();
        List<MultipleArticle> GetAllMultiple();
        List<ParticularArticle> GetParticularOnStorage();
        List<MultipleArticle> GetMultipleOnStorage();

        void SaveDbChanges();
        Task<List<ParticularArticle>> GetParticular(List<int> ids);
        Task<List<MultipleArticle>> GetMultiple(List<int> ids);
        Task<List<Article>> GetArticles(List<int> ids);
    }


    public class ArticleRepository : IArticleRepository
    {

        private StorageContext db;

        public ArticleRepository(StorageContext context)
        {
            db = context;
        }

        public int GenericId()
        {
            if ((db.ParticularArticles.Count() == 0) &&
                (db.MultipleArticles.Count() == 0))
                return 1;
            //return ((db.ParticularArticles.Select(q => q.RegNum)
            //    .Union(db.MultipleArticles.Select(q => q.RegNum))).Max() + 1);
            var regs = (db.ParticularArticles.Select(q => q.RegNum)
                        .Union(db.MultipleArticles.Select(q => q.RegNum)));
            int i = 0;
            while (true)
            {
                i++;
                if (!regs.Contains(i))
                    break;
            }
            return i;
        }

        public int GenericId(int? regnum = null)
        {
            if ((!regnum.HasValue) || (regnum.Value <= 0))
                return GenericId();
            var itemsCount = db.ParticularArticles.Where(q => q.RegNum == regnum.Value).Count() +
                db.MultipleArticles.Where(q => q.RegNum == regnum.Value).Count();
            if (itemsCount == 0)
                return regnum.Value;
            else return GenericId();
        }


        public void InsertParticular(ParticularArticle pa)
            => db.ParticularArticles.Add(pa);
        public ParticularArticle GetParticular(int reg)
            => (ParticularArticle)db.ParticularArticles.FirstOrDefault(x => x.RegNum == reg);
        public List<ParticularArticle> GetAllParticular()
            => db.ParticularArticles.ToList<ParticularArticle>();



        public List<ParticularArticle> GetParticularOnStorage ()
            => db.ParticularArticles.Where(x => x.RentId == null && !x.BlackMark).ToList<ParticularArticle>();


        public void InsertMultiple(MultipleArticle ma)
            => db.MultipleArticles.Add(ma);
        public MultipleArticle GetMultiple(int reg)
            => (MultipleArticle)db.MultipleArticles.FirstOrDefault(x => x.RegNum == reg);

        public List<MultipleArticle> GetAllMultiple()
            => db.MultipleArticles.ToList<MultipleArticle>();
        public List<MultipleArticle> GetMultipleOnStorage()
            => db.MultipleArticles.Where(x => x.NowCount > 0).ToList<MultipleArticle>();


        public void InsertCategory(Category categoryName)
            => db.Categories.Add(categoryName);
        public Category GetCategory(string categoryName)
            => db.Categories.FirstOrDefault(q => q.Name == categoryName);
        public List<Category> GetAllCategory() => db.Categories.ToList();

        public void SaveDbChanges() => db.SaveChanges();

        public async Task<List<ParticularArticle>> GetParticular(List<int> ids)
        {
            return await db.ParticularArticles.Where(q => ids.Contains(q.RegNum)).ToListAsync();
        }

        public async Task<List<MultipleArticle>> GetMultiple(List<int> ids)
        {
            return await db.MultipleArticles.Where(q => ids.Contains(q.RegNum)).ToListAsync();
        }

        public async Task<List<Article>> GetArticles(List<int> ids)
        {
            var partTask = await GetParticular(ids);
            var mulTask = await GetMultiple(ids);

            var list = new List<Article>( partTask);
            list.AddRange(mulTask);

            return list;
        }
    }
}

