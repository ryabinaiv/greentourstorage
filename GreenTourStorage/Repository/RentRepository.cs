﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenTourStorage.Models;
using GreenTourStorage.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace GreenTourStorage.Repository
{
    public interface IRentRepository
    {
        int GenericId();
        void InsertRent(Rent rent);
        List<Rent> GetArticleRentStory(int reg);
        Task<Rent> GetRentById(int id);
        Task<List<Rent>> GetAllRents();
        void SaveDbChanges();
        IDbContextTransaction startTransaction();
    }

    public class RentRepository : IRentRepository
    {
        private readonly StorageContext _db;
        private int _actualId;

        public RentRepository (StorageContext context)
        {
            _db = context;
            _actualId = !_db.Rents.Any() ? 0 : _db.Rents.Max(x => x.RentId);
        }

        public int GenericId() => ++_actualId;
        public void InsertRent(Rent rent)
        {
            _db.Rents.Add(rent);
           // _db.RentItems.AddRange(rent.RentItems);
        }

        //public void InsertRent (Rent rent) => _db.Rents.Add(rent);

        public List<Rent> GetArticleRentStory(int reg)
        {
            var result = new List<Rent>();
            //todo clarification is requied for next code
            // foreach (var rent in _db.Rents)
            // {
            //     foreach (var item in rent.RentItems.GetParticularArticlesList())
            //         if (item.RegNum == reg)
            //             result.Add(rent);
            //     foreach (var item in rent.RentItems.GetMultipleArticleSetsList())
            //         if (item.Article.RegNum == reg)
            //             result.Add(rent);
            // }
            return result
                //.OrderBy(x => x.RentBeginData)
                .ToList<Rent>();
            return result;
        }

        public async Task<Rent> GetRentById(int id)
            => await _db.Rents.Include(q=>q.RentItems)
                .FirstOrDefaultAsync(x => x.RentId == id);

        public async Task<List<Rent>> GetAllRents() => await _db.Rents.ToListAsync();

        public void SaveDbChanges() => _db.SaveChanges();
        public IDbContextTransaction startTransaction()
        {
            return _db.Database.BeginTransaction();
        }
    }
}
