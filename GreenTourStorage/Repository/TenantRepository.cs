﻿using System.Collections.Generic;
using System.Linq;
using GreenTourStorage.Models;
using GreenTourStorage.Infrastructure;

namespace GreenTourStorage.Repository
{
    public interface ITenantRepository
    {
        void InsertTenant(Tenant tenant);
        List<Tenant> GetAllTenants();
        Tenant GetTenantById(int id);
        Tenant GetTenantByPhone(string phone);
        void SaveDbChanges();
        void Update(Tenant tenant);
    }

    public class TenantRepository : ITenantRepository
    {
        private readonly StorageContext _db;

        public TenantRepository (StorageContext context)
        {
            _db = context;
        }

        public void InsertTenant(Tenant tenant) => _db.Tenants.Add(tenant);

        public List<Tenant> GetAllTenants() => _db.Tenants.ToList();

        public Tenant GetTenantById(int id) => _db.Tenants.FirstOrDefault(x => x.Id == id);

        public Tenant GetTenantByPhone(string phone) => _db.Tenants.FirstOrDefault(x => x.Phone == phone);

        public void SaveDbChanges() => _db.SaveChanges();
        public void Update(Tenant tenant)
        {
            _db.Tenants.Update(tenant);
        }
    }
}
