﻿using System;
using System.Collections.Generic;
using System.Linq;
using GreenTourStorage.Repository;
using GreenTourStorage.Models;

namespace GreenTourStorage.Services
{

    public class ArticleService: IArticleService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticleService(IArticleRepository article)
        {
            _articleRepository = article;
        }

        public void CreateCategory(string categoryName)
        {
            if (categoryName == null)
                throw new NullReferenceException();
            if (categoryName == string.Empty)
                throw new ArgumentOutOfRangeException();
            if (_articleRepository.GetCategory(categoryName) != null)
                throw new ArgumentOutOfRangeException("Такая категория уже существует");

            var newCategory = new Category(categoryName);
            _articleRepository.InsertCategory(newCategory);
            _articleRepository.SaveDbChanges();
        }

        public List<Category> GetAllCategories() => _articleRepository.GetAllCategory();

        public int RegisterParticularArticle(string name, string category, double price, string desc,
                                                            double? weight = null, int? regNum = null,
                                                            bool blackMark = false, string comment = null)
        {
            var categoryId = _articleRepository.GetCategory(category).CategoryId;
            var regId = _articleRepository.GenericId(regNum);
            _articleRepository.InsertParticular
                (new ParticularArticle(regId, name, category, price, desc, weight, comment, blackMark));
            _articleRepository.SaveDbChanges();
            return regId;
        }

        public ParticularArticle GetParticularArticle(int reg)
            => _articleRepository.GetParticular(reg);

        public void WriteOfParticularArticle(int id)
        {
            var pa = _articleRepository.GetParticular(id);

            if (pa == null)
                throw new NullReferenceException();
            if (pa.InRent)
                throw new ArgumentOutOfRangeException("Нельзя списать находящееся в аренде снаряжение");

            pa.WriteOf();
            _articleRepository.SaveDbChanges();
        }



        public int RegisterMultipleArticle (string name, string category, double price, int count, string desc, double? weight)
        {
            var id = _articleRepository.GenericId();
            _articleRepository.InsertMultiple(new MultipleArticle(
                id, name, category, price, count, desc, weight));
            _articleRepository.SaveDbChanges();
            return id;
        }

        public MultipleArticle GetMultipleArticle(int reg)
            => _articleRepository.GetMultiple(reg);

        // public void WriteOfMultipleSet(int id, int setId) {
        //     var ma = _articleRepository.GetMultiple(id);
        //     if (ma == null)
        //     {
        //         throw new NotImplementedException();
        //     }
        //     if (ma.GetSetsList().All(x => x.Id != setId))
        //     {
        //         throw new NotImplementedException();
        //     }
        //     ma.WriteOfSet(setId);
        //     _articleRepository.SaveDbChanges();
        // }

        public void RegisterAdditiveMultipleArticle(int id, int addCount)
        {
            var ma = _articleRepository.GetMultiple(id);

            if (ma == null)
                throw new NullReferenceException();
            if (addCount < 0)
                throw new ArgumentOutOfRangeException("Отрицательное значение");

            ma.IncreaseArticleCount(addCount);
            _articleRepository.SaveDbChanges();
        }

        public void EditArticle(int id, string name, double price, string description, string category, double? weight = null)
        {
            if ((name == null) || (name == ""))
                throw new NullReferenceException();
            if (price < 0)
                throw new ArgumentOutOfRangeException("Отрицательное значение");

            var ma = _articleRepository.GetMultiple(id);
            if ((ma != null))
            {

                ma.Weight = (decimal?)weight;
                ma.Name = name;
                ma.Price = (decimal)price;
                ma.Description = description;
                ma.Category = category;
            }

            var pa = _articleRepository.GetParticular(id);
            if (pa != null)
            {

                pa.Weight =(decimal?) weight;
                pa.Name = name;
                pa.Price = (decimal) price;
                pa.Description = description;
                pa.Category = category;
            }

            if ((pa == null) && (ma == null))
                throw new NullReferenceException();

            _articleRepository.SaveDbChanges();
        }

        public void SetBlackMarkParticularArticle(int reg, bool bm, string comment = null)
        {
            var pa = _articleRepository.GetParticular(reg);
            if (bm)
                pa.AddBlackMark(comment);
            else
                pa.CancelBlackMark();
            _articleRepository.SaveDbChanges();
        }
        public void SetWeightParticularArticle(int reg, decimal weight)
        {
            if (weight < 0)
                throw new ArgumentOutOfRangeException("Отрицательное значение");
            var pa = _articleRepository.GetParticular(reg);
            pa.Weight = weight;
            _articleRepository.SaveDbChanges();
        }

        public void SetWeightMultipleArticle(int reg, decimal weight)
        {
            if (weight < 0)
                throw new ArgumentOutOfRangeException("Отрицательное значение");
            var ma = _articleRepository.GetMultiple(reg);
            ma.Weight = weight;
            _articleRepository.SaveDbChanges();
        }

        public List<ParticularArticle> GetAllParticular()
            => _articleRepository.GetAllParticular();

        public List<MultipleArticle> GetAllMultiple()
            => _articleRepository.GetAllMultiple();

        public List<ParticularArticle> GetParticularOnStorage()
            => _articleRepository.GetParticularOnStorage();

        public List<MultipleArticle> GetMultipleOnStorage()
            => _articleRepository.GetMultipleOnStorage();
    }
}

