﻿using System.Collections.Generic;
using GreenTourStorage.Models;

namespace GreenTourStorage.Services
{
    public interface IArticleService
    {
        void SetBlackMarkParticularArticle(int reg, bool bm, string comment = null);
        void SetWeightParticularArticle(int reg, decimal weight);
        void SetWeightMultipleArticle(int reg, decimal weight);
        void CreateCategory(string categoryName);
        List<Category> GetAllCategories();

        int RegisterParticularArticle(string name, string category, double price, string desc, double? weight = null,
            int? regNum = null, bool blackMark = false, string comment = null);
        ParticularArticle GetParticularArticle(int reg);
        void WriteOfParticularArticle(int id);
        int RegisterMultipleArticle (string name, string category, double price, int count, string desc, double? weight);
        MultipleArticle GetMultipleArticle(int reg);
        // void WriteOfMultipleSet(int id, int setId);
        void RegisterAdditiveMultipleArticle(int id, int addCount);
        void EditArticle(int id, string name, double price, string description, string category, double? weight = null);
        List<ParticularArticle> GetAllParticular();
        List<MultipleArticle> GetAllMultiple();
        List<ParticularArticle> GetParticularOnStorage();
        List<MultipleArticle> GetMultipleOnStorage();
    }
}
