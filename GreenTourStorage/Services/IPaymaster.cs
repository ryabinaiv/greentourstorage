﻿using System.Collections.Generic;
using GreenTourStorage.Dtos;
using GreenTourStorage.Models;

namespace GreenTourStorage.Services
{
    public interface IPaymaster
    {
        public bool IsClubman(int id);
        public Tenant AddTenant (TenantCreateDto tenantDto);
        public void TakeInClub (Tenant tenant);
        public void RemoveClubman(Tenant tenant);
        public IList<Tenant> GetAllTenants();
        public Tenant GetTenant(in int reg);
        public Tenant Update(TenantDto tenant);
        Tenant BanTenant(in int id);
        Tenant KickOutTenant(in int id);
        object TakeInTenant(in int id);
    }
}
