﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenTourStorage.Controllers;
using GreenTourStorage.Models;

namespace GreenTourStorage.Services
{
    public interface IStorekeeper
    {
        public Task<List<Rent>> GetAllRents();
        public void ReturnParticularWithBlackMark(int paId, string comment);
        public void ReturnMultipleWithBlackMark(int maId, int maSetId, string comment); // TODO!!! int count
        public void FixIt(int maId, int maSetId);
        public void FixIt(int paId);
        public Task<Rent> CreateRent(CreateRentDto dto);
        public Task FinishRent(FinishRentDto rentId);
        public List<Rent> GetArticleRentStory (int reg);
        public Rent GetRentById(int id);
    }
}
