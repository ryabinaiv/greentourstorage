﻿using System;
using System.Collections.Generic;
using GreenTourStorage.Dtos;
using GreenTourStorage.Repository;
using GreenTourStorage.Models;


namespace GreenTourStorage.Services
{
    public class Paymaster: IPaymaster
    {
        private readonly ITenantRepository _tenantRepository;

        public Paymaster (ITenantRepository repository)
        {
            _tenantRepository = repository;
        }

        public bool IsClubman(int id)
        {
            var tenant = _tenantRepository.GetTenantById(id);
            if (tenant == null)
                throw new NullReferenceException();
            return tenant.IsClubman;
        }

        public Tenant AddTenant(TenantCreateDto tenantDto)
        {
            var tenant = new Tenant(tenantDto.Phone, tenantDto.NickName, tenantDto.IsClubman);
            _tenantRepository.InsertTenant(tenant);
            _tenantRepository.SaveDbChanges();
            return tenant;
        }

        public int AddClubman (string phone)
        {
            if (_tenantRepository.GetTenantByPhone(phone) != null)
                throw new ArgumentOutOfRangeException("Учатник с таким номером телефона уже сущетвует");
            var tenant = new Tenant(phone, "");
            _tenantRepository.InsertTenant(tenant);
            tenant.TakeInClub();
            _tenantRepository.SaveDbChanges();
            return tenant.Id;
        }

        public void TakeInClub (Tenant tenant)
        {
            tenant.TakeInClub();
            _tenantRepository.SaveDbChanges();
        }

        public void RemoveClubman(Tenant tenant)
        {
            tenant.TakeOffClub();
            _tenantRepository.SaveDbChanges();
        }

        public IList<Tenant> GetAllTenants() => _tenantRepository.GetAllTenants();
        public Tenant GetTenant(in int reg)
        {
            return _tenantRepository.GetTenantById(reg);
        }

        public void Update(Tenant tenant)
        {
            throw new NotImplementedException();
        }

        public Tenant BanTenant(in int id)
        {
            var tenant = GetTenant(id);
            tenant.BanTenant();
            _tenantRepository.Update(tenant);
            _tenantRepository.SaveDbChanges();
            return tenant;
        }

        public Tenant KickOutTenant(in int id)
        {
            var tenant = GetTenant(id);
            tenant.TakeOffClub();
            _tenantRepository.Update(tenant);
            _tenantRepository.SaveDbChanges();
            return tenant;
        }

        public object TakeInTenant(in int id)
        {
            var tenant = GetTenant(id);
            tenant.TakeInClub();
            _tenantRepository.Update(tenant);
            _tenantRepository.SaveDbChanges();
            return tenant;
        }

        public Tenant Update(TenantDto tenantDto)
        {
            var tenant = _tenantRepository.GetTenantById(tenantDto.Id);

            tenant.Phone = tenantDto.Phone;
            tenant.Nickname = tenantDto.NickName;
            _tenantRepository.Update(tenant);
            _tenantRepository.SaveDbChanges();
            return tenant;
        }
    }
}
