﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GreenTourStorage.Controllers;
using GreenTourStorage.Repository;
using GreenTourStorage.Models;

namespace GreenTourStorage.Services
{
    public class Storekeeper: IStorekeeper
    {
        private readonly IArticleRepository _articleRepositoryRepository;
        private readonly IRentRepository _rentRepositoryRepository;
        private readonly ITenantRepository _tenantRepository;

        public Storekeeper(IArticleRepository articleRepository, IRentRepository rentRepository,
            ITenantRepository tenantRepository)
        {
            _articleRepositoryRepository = articleRepository;
            _rentRepositoryRepository = rentRepository;
            _tenantRepository = tenantRepository;
        }

        public async Task<List<Rent>> GetAllRents() => await _rentRepositoryRepository.GetAllRents();

        public void ReturnParticularWithBlackMark(int paId, string comment)
        {
            var pa = _articleRepositoryRepository.GetParticular(paId);
            if (pa == null)
                throw new NullReferenceException();
            pa.AddBlackMark(comment);
            _articleRepositoryRepository.SaveDbChanges();
        }

        public void ReturnMultipleWithBlackMark(int maId, int maSetId, string comment) // TODO!!! int count
        {
            var ma = _articleRepositoryRepository.GetMultiple(maId);
            if (ma == null)
                throw new NullReferenceException();
            ma.ReturnWithBlackMark(maSetId, comment);
            _articleRepositoryRepository.SaveDbChanges();
        }

        public void FixIt(int maId, int maSetId)
        {
            var ma = _articleRepositoryRepository.GetMultiple(maId);
            if (ma == null)
                throw new NullReferenceException();
            ma.FixSet(maSetId);
            _articleRepositoryRepository.SaveDbChanges();
        }

        public void FixIt(int paId)
        {
            var pa = _articleRepositoryRepository.GetParticular(paId);
            if (pa == null)
                throw new NullReferenceException();
            if (pa.IsWriteOf)
                throw new ArgumentOutOfRangeException("Нельзя починить списанное снаряжение");
            pa.CancelBlackMark();
            _articleRepositoryRepository.SaveDbChanges();
        }

        public async Task<Rent> CreateRent(CreateRentDto dto)
        {
            var tenant = _tenantRepository.GetTenantByPhone(dto.User);

            if (tenant == null)
            {
                //todo вынести в сервис
                tenant = new Tenant(dto.User, "");
                _tenantRepository.InsertTenant(tenant);
                _tenantRepository.SaveDbChanges();
            }

            if (tenant.IsBanned)
            {
                throw new ArgumentException("Пользователь забанен");
            }

            dto.Items = dto.Items.Where(q => q.RequestedCount != 0).ToArray();

            if (dto.Items.Length == 0)
            {
                throw new ArgumentException("Отсутвуют товары для аренды");
            }

            var  articles =(await _articleRepositoryRepository.GetArticles(
                dto.Items.Select(q => q.ArticleId).ToList())
                    ).ToDictionary(q=>q.RegNum, q=>q);

            var rent = new Rent(_rentRepositoryRepository.GenericId(), tenant, "");
            var transaction = _rentRepositoryRepository.startTransaction();
            try
            {
                foreach (var item in dto.Items)
                {
                    rent.AddArticleInRent(articles[item.ArticleId], item.RequestedCount);
                    articles[item.ArticleId].GoToRent((rent.RentId, item.RequestedCount));
                }

                 rent.MakePay(rent.RentCostInWeek);
                 _rentRepositoryRepository.InsertRent(rent);
                 _rentRepositoryRepository.SaveDbChanges();

                 _articleRepositoryRepository.SaveDbChanges();
                 await transaction.CommitAsync();
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                Console.WriteLine(e);
                throw e;
            }
            return rent; //TO DO  еще нужно вернуть ид аренды и стоимость
        }

        public async Task FinishRent(FinishRentDto dto)
        {
            var rent = await _rentRepositoryRepository.GetRentById(dto.RentId);
            if (rent == null)
                throw new NullReferenceException();

            if (rent.IsFinished)
                throw new ArgumentException("Rent is finished");

            var transaction = _rentRepositoryRepository.startTransaction();
            try
            {
                rent.FinishRent(dto);

                await UpdateStocks(rent);

                _rentRepositoryRepository.SaveDbChanges();
                _articleRepositoryRepository.SaveDbChanges();

                await transaction.CommitAsync();

                if (rent.RentItems.Any(q => q.Returned != q.Count))
                {
                    var createNewDto = new CreateRentDto
                    {
                        User = _tenantRepository.GetTenantById(rent.TenantId).Phone,
                        Items = rent.RentItems.Select(q => new CreateRentItemDto()
                        {
                            ArticleId = q.ArticlesId,
                            RequestedCount = q.Count - q.Returned
                        }).Where(q=> q.RequestedCount != 0)
                            .ToArray()
                    };

                    await CreateRent(createNewDto);
                }
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync();
                Console.WriteLine(e);
                throw e;
            }
        }

        private async Task UpdateStocks(Rent rent)
        {
            var articles =
                (await _articleRepositoryRepository.GetArticles(
                    rent.RentItems.Select(q => q.ArticlesId).ToList()))
                .ToDictionary(q => q.RegNum, q => q);

            foreach (var item in rent.RentItems)
            {
                articles[item.ArticlesId].ReturnToStorage(item);
            }

            _articleRepositoryRepository.SaveDbChanges();
        }

        public List<Rent> GetArticleRentStory (int reg) => _rentRepositoryRepository.GetArticleRentStory(reg);

        public Rent GetRentById(int id) => _rentRepositoryRepository.GetRentById(id).Result;
    }
}

