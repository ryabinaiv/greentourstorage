﻿using AutoMapper;
using GreenTourStorage.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using GreenTourStorage.Infrastructure;
using GreenTourStorage.Mappers;
using GreenTourStorage.Queries;
using GreenTourStorage.Repository;
using GreenTourStorage.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Events;

 namespace GreenTourStorage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration["ConnectionString"];

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false; //TODO switch of after
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // укзывает, будет ли валидироваться издатель при валидации токена
                        ValidateIssuer = true,
                        // строка, представляющая издателя
                        ValidIssuer = AuthOptions.ISSUER,

                        // будет ли валидироваться потребитель токена
                        ValidateAudience = true,
                        // установка потребителя токена
                        ValidAudience = AuthOptions.AUDIENCE,
                        // будет ли валидироваться время существования
                        ValidateLifetime = true,

                        // установка ключа безопасности
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                        // валидация ключа безопасности
                        ValidateIssuerSigningKey = true,
                    };
                });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });

            // services.AddAuthorization(opts => {
            //     opts.AddPolicy("OnlyForLondon", policy => {
            //         policy.RequireClaim(ClaimTypes.Locality, "Лондон", "London");
            //     });
            //     opts.AddPolicy("OnlyForMicrosoft", policy => {
            //         policy.RequireClaim("company", "Microsoft");
            //     });
            // });


            services.AddDbContext<StorageContext>(options =>  options.UseSqlServer(connection));


            services.AddTransient<IArticleRepository, ArticleRepository>();
            services.AddTransient<IRentRepository, RentRepository>();
            services.AddTransient<ITenantRepository, TenantRepository>();
            services.AddTransient<IPaymaster, Paymaster>();
            services.AddTransient<IStorekeeper, Storekeeper>();
            services.AddTransient<IArticleService, ArticleService>();

            services.AddTransient<IRentQueries, RentQueries>();
            services.AddTransient<IArticleQueries, ArticleQueries>();

            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainProfile());
            }).CreateMapper());


            services.AddMvc(r => r.EnableEndpointRouting = false);

            services.Configure<AccountSettings>(Configuration.GetSection("Account"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                // установка обработчика ошибок
                app.UseExceptionHandler("/Home/Error");
            }
            // установка обработчика статических файлов
            app.UseStaticFiles();
            //addded serilog
            app.UseSerilogRequestLogging();
            //предоставляет поддержку аутентификации
            app.UseAuthentication();
            app.UseAuthorization();


            //устанавливает компоненты MVC для обработки запроса и, в частности, настраивает систему маршрутизации в приложении
             app.UseMvc(
                 routes =>
             {
                 routes.MapRoute(
                     name: "default",
                     template: "{controller=Home}/{action=Index}/{id?}");


            }
        );

            // app.UseEndpoints(endpoints =>
            // {
            //     endpoints.MapControllers();     // нет определенных маршрутов
            // });
        }
    }
}
