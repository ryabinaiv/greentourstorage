﻿using System;

namespace GreenTourStorage.ViewModels
{
    public class MultipleArticleViewModel
    {
        public int RegNum { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }


        public string Description { get; set; }
        public decimal? Weight { get; set; }
        public int TotalCount { get;  set; }
        public int NowCount { get;  set; }
        public int InRepairCount { get; set; }
        public MultipleArticleRentStoryItem[] InRent { get; set; }
    }

    public class MultipleArticleRentStoryItem
    {
        public int RentId { get; set; }
        public DateTime RentDateTime { get; set; }
        public string Tenant { get; set; }
        public decimal Count { get; set; }
    }
}
