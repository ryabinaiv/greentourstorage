﻿using System;

namespace GreenTourStorage.ViewModels
{
    public class RentListItemViewModel
    {
        public int RenId { get; set; }
        public int TetantId { get; set; }
        public string TetantName { get; set; }
        public DateTime RentDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public decimal PaidSum { get; set; }
        public decimal Weeks { get; set; }
        public bool IsFinished { get; set; }
    }
}
