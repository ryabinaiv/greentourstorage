﻿using System;
using System.Collections;
using System.Collections.Generic;
using GreenTourStorage.Repository;

namespace GreenTourStorage.ViewModels
{
    public class RentViewModel
    {
        public int RenId { get; set; }
        public int TetantId { get; set; }
        public string TetantName { get; set; }
        public DateTime RentDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public decimal PaidSum { get; set; }
        public decimal Weeks { get; set; }
        public decimal FinalCost { get; set; }
        public bool IsFinished { get; set; }
        public IList<RentItemViewModel> Items { get; set; }
    }

    public class RentItemViewModel
    {
        public int RentId { get;  set; }
        public int ArticlesId { get;   set;}
        public decimal Price { get;  set; }
        public int Count { get;  set; }
        public string ArticlesName { get; set; }
        public ArticleType Type { get; set; }
        public int ToRepair { get; set; }
        public int Returned { get; set; }
    }
}
