﻿namespace GreenTourStorage.ViewModels
{
    public class TenantDto
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public bool IsClubman { get; set; }

        public bool IsBanned { get;  set; }
    }
}
