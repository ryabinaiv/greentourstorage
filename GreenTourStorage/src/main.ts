﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

import {TenantModule} from "./tenant/tenant.module";
import {MaskedInputModule} from "./ui-items/MaskedInput/masked-input.module";
const platform = platformBrowserDynamic();
//platform.bootstrapModule(AppModule);
platform.bootstrapModule(TenantModule);
//platform.bootstrapModule(MaskedInputModule)
  //.finally();
