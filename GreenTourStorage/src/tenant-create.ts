﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

import {TenantModule} from "./tenant/tenant.module";
import {MaskedInputModule} from "./ui-items/MaskedInput/masked-input.module";
import {TenantAddComponent} from "./tenant/tenant-add.component";
import {CreateTenantModule} from "./tenant/create-tenant.module";
const platform = platformBrowserDynamic();
//platform.bootstrapModule(AppModule);
platform.bootstrapModule(CreateTenantModule);
//platform.bootstrapModule(MaskedInputModule)
  //.finally();
