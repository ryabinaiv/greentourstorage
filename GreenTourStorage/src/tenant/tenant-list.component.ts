﻿import {TenantService} from "./tenant.service";
import {Component, OnInit} from "@angular/core";
import {Tenant} from "./tenant";

@Component({
  selector: 'tenant',
  templateUrl: './tenant-list.component.html',

})
export class TenantListComponent implements OnInit {
  constructor(
    private tenantService: TenantService
  ) { }

  tenants: Tenant[];
  editingTenant: Tenant;

  ngOnInit(): void {
     this.loadTenants();
  }

  private loadTenants(): void  {
    this.tenantService.getTenants().subscribe(
     q => this.tenants = q
    );
  }
  //
  public cancel(): void {
    this.editingTenant = null;
  }
  //

  public editTenant(t: Tenant): void{
    this.editingTenant = t;
  }

  public banTenant(t: Tenant): void{
    this.tenantService.banTenant(t).subscribe(
      q => this.loadTenants()
    );
  }

  public kickOutTenant(t: Tenant): void {
    this.tenantService.kickOutTenant(t).subscribe(
      q => this.loadTenants()
    );
  }
  public takeInTenant(t: Tenant): void {
    this.tenantService.takeInTenant(t).subscribe(
      q => this.loadTenants()
    );
  }

  public save() {
    console.log("save"+JSON.stringify(this.editingTenant));
    this.tenantService.updateTenant(this.editingTenant).subscribe(
      q => {
        this.loadTenants();
        this.editingTenant = null;
      }
    );
  }


  //
  // public delete(t: Tenant): void {
  //   // this.tenantService.deleteTenant(t.id)
  //   //   .subscribe(data => this.loadTenants());
  // }
  //
  // public editProduct(t: Tenant) {
  //   this.tenant = t;
  // }
  //

}
