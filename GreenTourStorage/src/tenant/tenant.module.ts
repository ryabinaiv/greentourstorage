﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TenantListComponent } from './tenant-list.component';
import {TenantService} from './tenant.service';
import {TenantAddComponent} from './tenant-add.component';
import {RouterModule, Routes} from "@angular/router";
import { ReactiveFormsModule } from '@angular/forms';

import { AngularFormsInputMasksModule } from 'angular-forms-input-masks';
import {TextMaskModule} from "angular2-text-mask";
import {MaskedInputModule} from "../ui-items/MaskedInput/masked-input.module";




// определение маршрутов
const appRoutes: Routes = [
  // { path: '', component: ProductListComponent },
  // { path: 'product/:id', component: ProductDetailComponent },
 // { path: '**', redirectTo: '/' }
]
@NgModule({
  imports: [BrowserModule, FormsModule, HttpClientModule, AngularFormsInputMasksModule,
    ReactiveFormsModule, TextMaskModule, MaskedInputModule],
  declarations: [TenantListComponent,// TenantAddComponent
  ],
  bootstrap: [
    TenantListComponent,
   // TenantAddComponent
  ],
  providers: [TenantService]
})
export class TenantModule { }
