﻿import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Tenant, TenantCreatrDto} from "./tenant";
import {Observable} from "rxjs";

@Injectable()
export class TenantService {

  private url = "/api/v1/Tenant";

  constructor(private http: HttpClient) {
  }

  getTenants(): Observable<Tenant[]> {
    return <Observable<Tenant[]>> this.http.get(this.url);
  }

  getTenant(id: number): Observable<Tenant> {
    return <Observable<Tenant>> this.http.get(this.url + '/' + id);
  }

  public createTenant(tenant: TenantCreatrDto): Observable<Tenant> {
    return <Observable<Tenant>> this.http.post(this.url, tenant);
  }

  updateTenant(tenant: Tenant): Observable<Tenant> {
    return <Observable<Tenant>> this.http.put(`${this.url}/${tenant.id}`, tenant);
  }


  kickOutTenant(t: Tenant): Observable<Tenant> {
    return <Observable<Tenant>> this.http.put(`${this.url}/${t.id}/kickOutTenant`,{});
  }

  banTenant(t: Tenant): Observable<Tenant> {
    return <Observable<Tenant>> this.http.put(`${this.url}/${t.id}/banTenant`,{});
  }

  takeInTenant(t: Tenant): Observable<Tenant> {
    return <Observable<Tenant>> this.http.put(`${this.url}/${t.id}/takeInTenant`,{});
  }
}
