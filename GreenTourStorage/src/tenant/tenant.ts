﻿export class Tenant {

  constructor(
    public id: number,
    public phone: string,
    public state: TenantType,
    public nickName?: string,
    ) { }
}

export class TenantCreatrDto {
  public phone: string;
  public isClubman: string;
  public nickName?: string;
}

export enum TenantType
{
  Simple="Simple",
  Banned="Banned",
  Clubman="Clubman",
}
