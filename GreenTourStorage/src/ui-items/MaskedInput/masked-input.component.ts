﻿import {Component} from "@angular/core";

@Component({
  selector: 'maskedInput',
  template: `
      <input type="text"  class="form-control"
             prefix="+7" angularFormsMask="D-(DDD)-DDD-DD-DD"
             placeholder="8-(908)-864-55-91"
             [(ngModel)]="myModel"
             [validateMaskInput]="true">
  `
})
export class MaskedInputComponent {
  public myModel = '+79545'
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
}



