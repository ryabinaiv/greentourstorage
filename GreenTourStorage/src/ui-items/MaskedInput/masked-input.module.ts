﻿import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import {MaskedInputComponent} from "./masked-input.component";
import {AngularFormsInputMasksModule} from "angular-forms-input-masks";
import {TenantListComponent} from "../../tenant/tenant-list.component";

@NgModule({
  imports: [
    FormsModule,
    TextMaskModule,
    AngularFormsInputMasksModule
  ],
  declarations: [MaskedInputComponent],
  // exports: [
  //   MaskedInputComponent
  // ],
  // bootstrap: [MaskedInputComponent],
  //providers: [MaskedInputComponent]
})
export class MaskedInputModule {}
