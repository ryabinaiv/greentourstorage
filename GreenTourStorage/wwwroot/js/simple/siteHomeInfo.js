﻿
//Home/Info

function homeEditInfo(target) {
    let div = document.getElementById(target);
    let beforeText = div.children[2];
    let editSpan = document.getElementById(target + "EditSpan");
    let afterText = document.getElementById(target + "EditSpace");
    let editBtn = document.getElementById(target+'EditInfoBtn');
    editBtn.hidden = true; //почему-то не скрывает, хотя работает
    beforeText.hidden = true;
    editSpan.hidden = false;
    afterText.innerHTML = beforeText.innerHTML; //

    let okBtn = document.getElementById(target + 'OkBtn');
    okBtn.onclick = function () {
        beforeText.innerHTML = afterText.value;
        returnView();
    };

    let cslBtn = document.getElementById(target + 'CslBtn');
    cslBtn.onclick = function () {
        afterText.value = beforeText.innerHTML;
        returnView();
    }; 

    function returnView() {
        editSpan.hidden = true;
        editBtn.hidden = false;
        beforeText.hidden = false;
    }
}