﻿
//Rent/Index

function rentBegin() {
    rentGetNowRents();
    nowRentsCB.checked = true;
}

function rentGetNowRents() {
    let info = rentSearchSpace.value;
    if ((info == '') || (info == null))
        rentHideRows(true, false, false);
    else
        if (nowRentsCB.checked == true)
            rentHideRows(true, true, false);
        else
            rentHideRows(false, true, false);
    nowRentsLabel.classList.toggle('attention');
}

function rentGetSearchInfo() {
    if (nowRentsCB.checked == true)
        rentHideRows(true, true, false);
    else
        rentHideRows(false, true, false);
}

function rentDropSearch() {
    if (nowRentsCB.checked == true)
        rentHideRows(true, false, true);
    else
        rentHideRows(false, false, true);
    rentSearchSpace.value = null;
}

function rentHideRows(now, search, drop) {

    function searchInfo() {
        let info = rentSearchSpace.value.toLowerCase();
        if ((info == '') || (info == null))
            return;
        for (let item of Array.from(allRentsTable.rows).slice(1)) {
            item.hidden = true;
            for (let cell of item.children) {
                try {
                    for (let cc of Array.from(cell.firstElementChild.rows)) {
                        if (cc.innerHTML.toLowerCase().includes(info)) {
                            item.hidden = false;
                            cc.classList.add('attention');
                        }
                    }
                }
                catch (err) {
                    if (cell.firstElementChild == null) {
                        if (cell.innerHTML.toLowerCase().includes(info)) {
                            item.hidden = false;
                            cell.classList.add('attention');
                        }
                    }
                }  
            }
        }  
    }

    function getNowRents(search) {
        for (let item of Array.from(allRentsTable.rows).slice(1)) {
            if ((item.children[3].innerHTML != null) && (item.children[3].innerHTML != "")) {
                if (search && (item.hidden == true))
                    continue;
                item.hidden = item.hidden == true ? false : true;
            }
        }
    }

    function dropSearchInfo() {
        for (let item of Array.from(allRentsTable.rows).slice(1)) {
            for (let cell of item.children) {
                try {
                    let t = Array.from(cell.children[0].rows);
                    for (let cc of t)
                        cc.classList.remove('attention');
                }
                catch (err) {
                    cell.classList.remove('attention');
                } 
                item.hidden = false; 
            }
        }
    }

    if (search && drop)
        return;

    if (now && search) {
        searchInfo();
        getNowRents(true);
        return;
    }

    if (now && drop) {
        dropSearchInfo();
        getNowRents(false);
        return;
    }

    if (now)
        getNowRents(false);

    if (search) {
        dropSearchInfo();
        searchInfo();
    }

    if (drop)
        dropSearchInfo();
}


//Rent/BeginRent

function newRentGetSearchInfo() {
    let info = newRentSearchSpace.value;
    if ((info == '') || (info == null))
        return;
    for (let item of Array.from(articlesForRentTable.rows).slice(1)) {
        item.hidden = true;
        for (let cell of item.children) {
            if (cell.innerHTML.includes(info)) {
                item.hidden = false;
                cell.classList.add('attention');
            }
        }
    }
}

function newRentDropSearch() {
    for (let item of Array.from(articlesForRentTable.rows).slice(1)) {
        for (let cell of item.children)
            cell.classList.remove('attention');
        item.hidden = false;
    }
    newRentSearchSpace.value = null;
}

//Rent/EndRent