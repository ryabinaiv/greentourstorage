﻿
//Storage/Index

function storageBegin() {
    storageCreateAllArticlesTable();
}

function storageCreateAllArticlesTable() {
    let arrayPa = Array.from(particularArticlesTable.cloneNode(true).rows);
    let arrayMa = Array.from(multipleArticlesTable.cloneNode(true).rows);
    allArticlesTable.tBodies[0].append(...arrayPa);
    allArticlesTable.tBodies[0].append(...arrayMa);
    let head = allArticlesTable.children[0].innerHTML;
    particularArticlesTable.children[0].innerHTML = head;
    multipleArticlesTable.children[0].innerHTML = head;
}

function storageIntoAllArticlesTable() {
    allArticlesTable.tBodies[0].replaceWith(particularArticlesTable.tBodies[0].cloneNode(true));
    allArticlesTable.tBodies[0].append(...Array.from(multipleArticlesTable.cloneNode(true).rows).slice(1));
}

function storageGetArticlesOnStorage() {
    let info = searchSpace.value;
    if ((info == '') || (info == null))
        storageHideRows(true, false, false);
    else
        if (articlesOnStorageCB.checked == true)
            storageHideRows(true, true, false);
        else
            storageHideRows(false, true, false);
    onStorageLabel.classList.toggle('attention');
    storageIntoAllArticlesTable();
}

function storageGetSearchInfo() {
    if (articlesOnStorageCB.checked == true)
        storageHideRows(true, true, false);
    else
        storageHideRows(false, true, false);
    storageIntoAllArticlesTable();
}

function storageDropSearch() {
    if (articlesOnStorageCB.checked == true)
        storageHideRows(true, false, true);
    else
        storageHideRows(false, false, true);
    searchSpace.value = null;
    storageIntoAllArticlesTable();
}

function storageHideRows(storage, search, drop) {

    function searchInfo() {

        function searchInTable(table) {
            let info = searchSpace.value.toLowerCase();
            if ((info == '') || (info == null))
                return;
            for (let item of Array.from(table.rows).slice(1)) {
                item.hidden = true;
                for (let cell of item.children) {
                    if (cell.innerHTML.toLowerCase().includes(info)) {
                        item.hidden = false;
                        cell.classList.add('attention');
                    }
                }
            }
        }
        searchInTable(particularArticlesTable);
        searchInTable(multipleArticlesTable);
    }

    function getOnStorage(search) {
        for (let item of Array.from(particularArticlesTable.rows).slice(1)) {
            if ((item.children[9].innerHTML != null) && (item.children[9].innerHTML != "")) {
                if (search && (item.hidden == true))
                    continue;
                item.hidden = item.hidden == true ? false : true;
            }
        }
        for (let item of Array.from(multipleArticlesTable.rows).slice(1)) {
            if ((item.children[10].innerHTML == 0)) {
                if (search && (item.hidden == true))
                    continue;
                item.hidden = item.hidden == true ? false : true;
            }
        }
    }

    function dropSearchInfo() {

        function dropSearch(table) {
            for (let item of Array.from(table.rows).slice(1)) {
                for (let cell of item.children) {
                    item.hidden = false;
                    cell.classList.remove('attention');
                }
            }
        }
        dropSearch(particularArticlesTable);
        dropSearch(multipleArticlesTable);
    }

    if (search && drop)
        return;

    if (storage && search) {
        searchInfo();
        getOnStorage(true);
        return;
    }

    if (storage && drop) {
        dropSearchInfo();
        getOnStorage(false);
        return;
    }

    if (storage) 
        getOnStorage(false);

    if (search) {
        dropSearchInfo();
        searchInfo();
    }

    if (drop)
        dropSearchInfo();
}



//Storage/AddArticle

function storageShowAddDetails(name) {
    let details = document.getElementById(name);
    if (name == 'MaCreateDetails') {
        let detailsPa = document.getElementById('PaCreateDetails');
        detailsPa.hidden = true;
        details.hidden = false;
        let regNum = document.getElementById('regNum');
        regNum.hidden = true;
        let priceLabel = document.getElementById('priceLabel');
        priceLabel.textContent = "Стоимость (1 шт):";
        let weightLabel = document.getElementById('weightLabel');
        weightLabel.textContent = "Вес (1шт):";
    }
    if (name == 'PaCreateDetails') {
        let detailsMa = document.getElementById('MaCreateDetails');
        detailsMa.hidden = true;
        details.hidden = false;
        let maCount = document.getElementById('maCount');
        maCount.required = false;
        let regNum = document.getElementById('regNum');
        regNum.hidden = false;
        let priceLabel = document.getElementById('priceLabel');
        priceLabel.textContent = "Стоимость:";
        let weightLabel = document.getElementById('weightLabel');
        weightLabel.textContent = "Вес:";
        storageRequiredCommentForBM();
    }
}

function storageRequiredCommentForBM() {
    let bm = document.getElementById('blackMark');
    let comment = document.getElementById('comment');
    let commentplace = document.getElementById('commentplace');
    if (bm.checked) {
        comment.required = true;
        commentplace.hidden = false;
    }
    else {
        comment.required = false;
        commentplace.hidden = true;
    }
}

//Storage/ParticularArticle
function viewForBlackMark() {
    let comment = document.getElementById('comment');
    let bm = document.getElementById('realBlackMark');
    let bm_cb = document.getElementById('blackMark');
    let commentplace = document.getElementById('commentplace');
    if (bm_cb.checked) {
        commentplace.hidden = false;
        if (bm) {
            comment.disabled = false;
        }
        else {
            comment.required = true;
        }
    }
    else {
        commentplace.hidden = true;
        if (bm) {
            comment.disabled = true;
        }
        else {
            comment.required = false;
        }
    }
}

//Storage/Categories
// --> null