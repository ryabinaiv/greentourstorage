﻿
//Clubmen/Index

function tenantGetClubmen() {
    let info = tenantSearchSpace.value;
    if ((info == '') || (info == null))
        tenantHideRows(true, false, false);
    else
        if (clubmenCB.checked == true)
            tenantHideRows(true, true, false);
        else
            tenantHideRows(false, true, false);
    clubmenLabel.classList.toggle('attention');
}

function tenantGetSearchInfo() {
    if (clubmenCB.checked == true)
        tenantHideRows(true, true, false);
    else
        tenantHideRows(false, true, false);
}

function tenantDropSearch() {
    if (clubmenCB.checked == true)
        tenantHideRows(true, false, true);
    else
        tenantHideRows(false, false, true);
    tenantSearchSpace.value = null;
}

function tenantHideRows(clubmen, search, drop) {

    function searchInfo() {
        let info = tenantSearchSpace.value.toLowerCase();
        if ((info == '') || (info == null))
            return;
        for (let item of Array.from(tenantsTable.rows).slice(1)) {
            item.hidden = true;
            for (let cell of item.children) {
                if (cell.innerHTML.toLowerCase().includes(info)) {
                     item.hidden = false;
                     cell.classList.add('attention');
                }
            }
        }
    }

    function getClubmen(search) {
        for (let item of Array.from(tenantsTable.rows).slice(1)) {
            if (item.children[2].children[0].checked == false) {
                if (search && (item.hidden == true))
                    continue;
                item.hidden = item.hidden == true ? false : true;
            }
        }
    }

    function dropSearchInfo() {
        for (let item of Array.from(tenantsTable.rows).slice(1)) {
            for (let cell of item.children)
                    cell.classList.remove('attention');
            item.hidden = false;
        }
    }

    if (search && drop)
        return;

    if (clubmen && search) {
        searchInfo();
        getClubmen(true);
        return;
    }

    if (clubmen && drop) {
        dropSearchInfo();
        getClubmen(false);
        return;
    }

    if (clubmen)
        getClubmen(false);

    if (search) {
        dropSearchInfo();
        searchInfo();
    }

    if (drop)
        dropSearchInfo();
}
