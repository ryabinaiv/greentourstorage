﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using GreenTourStorage.Repository;
//using GreenTourStorage.Services;
//using GreenTourStorage.Models;

//namespace GreenTourStorageSimpleTests
//{
//    public class DomainTests
//    {
//        IArticleRepository article_repository;
//        IRentRepository rent_repository;
//        ITenantRepository tenant_repository;
//        Storekeeper storekeeper;
//        Paymaster paymaster;

//        public DomainTests (IArticleRepository iarticle, ITenantRepository itenant, IRentRepository irent)
//        {
//            article_repository = iarticle;
//            rent_repository = irent;
//            tenant_repository = itenant;
//            storekeeper = new Storekeeper(article_repository, rent_repository, tenant_repository);
//            paymaster = new Paymaster(tenant_repository);
//        }

//        public void StartArticleTest()
//        {
//            //storekeeper.CreateCategory("Палатки");
//            //storekeeper.CreateCategory("Альп. железо");

//            //storekeeper.RegisterParticularArticle("Палатка зимняя", "Палатки", 8000, "Зимняя 4х местная, красненькая", 5, 154);

//            //var id = storekeeper.RegisterMultipleArticle("Карабины муфтованные", "Альп. железо", 600, 20, "С зеленой краской");

//            //storekeeper.RegisterAdditiveMultipleArticle(id, 5);

//        }

//        public void StartTenantTest()
//        {
//            paymaster.AddClubman(new Tenant(tenant_repository.GenericId(), "+7-914-619-99-23"));
//        }

//        public void StartRentTest()
//        {
//            var all_pa = article_repository.GetAllParticular();
//            var all_ma = article_repository.GetAllMultiple();

//            //First Rent
//            var pa_id_list = new List<int>();
//            var ma_id_list = new Dictionary<int, int>();
//            foreach (var item in all_pa)
//            {
//                pa_id_list.Add(item.Id);
//            }
//            foreach (var item in all_ma)
//            {
//                ma_id_list.Add(item.Id, 10);
//            }
//            var rent1 = storekeeper.StartRent(pa_id_list, ma_id_list, tenant_repository.GetAllTenant().First<Tenant>(), "Зимний поход");
//            storekeeper.MakeFirstRentPay(rent1, 200);
//            //PrintArticles();

//            //Second Rent
//            ma_id_list.Clear();
//            foreach (var item in all_ma)
//            {
//                ma_id_list.Add(item.Id, 12);
//            }
//            var rent2 = storekeeper.StartRent(new List<int>(), ma_id_list, new Tenant(tenant_repository.GenericId(), "+7-965-999-65-85"));
//            storekeeper.MakeFirstRentPay(rent2, 600);
//            //PrintArticles();

//            //Finish Rents
//            storekeeper.ReturnParticularWithBlackMark(3, "Порвали, заразы");
//            var mes = storekeeper.FinishRent(rent1);
//            //Console.WriteLine($"ДОПЛАТИТЬ ЗА АРЕНДУ: {mes}");
//            //PrintLine();

//            storekeeper.ReturnMultipleWithBlackMark(4, 2, "Нужно мыть");
//            mes = storekeeper.FinishRent(rent2);
//            //Console.WriteLine($"ДОПЛАТИТЬ ЗА АРЕНДУ: {mes}");
//            storekeeper.WriteOfMultipleSet(4, 2);
//            //PrintLine();
//            //PrintArticles();

//            //Thirst Rent
//            var rent3 = storekeeper.StartRent(pa_id_list, ma_id_list, new Tenant(tenant_repository.GenericId(), "8-914-588-96-98"), "Пробуем");
//            storekeeper.MakeFirstRentPay(rent3, 900);
//            //PrintArticles();

//            mes = storekeeper.FinishRent(rent3);
//            //Console.WriteLine($"ДОПЛАТИТЬ ЗА АРЕНДУ: {mes}");
            
//            storekeeper.WriteOfParticularArticle(3);storekeeper.FixIt(3);
//            storekeeper.FixIt(4, 2);
//            var rent_history = rent_repository.GetArticleRentStory(3);
//            //PrintRents(rent_history);
//            rent_history = rent_repository.GetArticleRentStory(4);
//            //PrintRents(rent_history);
//        }

//        public void PrintRents()
//        {
//            Console.WriteLine("ВСЕ АРЕНДЫ:");
//            foreach (var i in rent_repository.GetAllRents())
//            {
//                Console.WriteLine($"      * ID({i.RentId}) - Взял({i.TenantId}) - Бесплатно({i.IsFreeRent}) - Комментарий({i.Comment}) - " +
//                    $"Начало({i.RentBeginData}) - Конец({i.RentEndData}) - Цена за неделю({i.RentWeekPrice})");
//                PrintParticular(i.RentItems.GetParticularArticlesList());
//                PrintMultipleSets(i.RentItems.GetMultipleArticleSetsList());
//                PrintPlusLine();
//            }
//            PrintLine();
//        }

//        public void PrintRents(List<Rent> list)
//        {
//            Console.WriteLine("История:");
//            foreach (var i in list)
//            {
//                Console.WriteLine($"+ ID({i.RentId}) - Взял({i.TenantId}) - Бесплатно({i.IsFreeRent}) - Комментарий({i.Comment}) - " +
//                    $"Начало({i.RentBeginData}) - Конец({i.RentEndData}) - Цена за неделю({i.RentWeekPrice})");
//            }
//            PrintLine();
//        }

//        public void PrintPlusLine() => Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//        public void PrintLine() => Console.WriteLine("_____________________________________________________________________");

//        public void PrintCategories()
//        {
//            Console.WriteLine("КАТЕГОРИИ:");
//            foreach (var i in article_repository.GetAllCategory())
//            {
//                Console.WriteLine($"ID({i.CategoryId}) - Категория({i.Name})");
//            }
//            PrintLine();
//        }

//        public void PrintArticles()
//        {
//            Console.WriteLine("ВЕЩИ НА СКЛАДЕ:");
//            PrintParticular(article_repository.GetAllParticular());
//            PrintMultiple(article_repository.GetAllMultiple());
//            PrintLine();
//        }

//        private void PrintParticular (List<ParticularArticle> list)
//        {
//            foreach (var i in list)
//            {
//                if (i == null) break;
//                Console.WriteLine($"ID({i.Id}) - {i.Name} - Категория({i.Category}) - Цена({i.Price}) " +
//                    $"- Описание({i.Description}) - Вес({i?.Weight}) - Находится у ({i?.TenantId}) " +
//                    $"- Сломано({i.BlackMark}) - Почему сломано({i?.Comment}) - Списано({i.IsWriteOf})");
//            }
//        }

//        private void PrintMultiple(List<MultipleArticle> list)
//        {
//            foreach (var i in list)
//            {
//                if (i == null) break;
//                Console.WriteLine($"ID({i.Id}) - {i.Name} - Категория({i.Category}) - Цена({i.Price}) " +
//                    $"- Описание({i.Description}) - Вес({i.Weight}) " +
//                    $"- Всего имеем({i.GetTotalCount()}) - Сейчас на складе({i.GetNowCount()})");
//                PrintMultipleSets(i.GetSetsList());
//            }
//        }

//        private void PrintMultipleSets(List<MultipleArticleSet> list)
//        {
//            foreach (var i in list)
//            {
//                if (i == null) break;
//                Console.WriteLine($"-----ID({i.Id}) - Артикль({i.ArticleId}) - Штук({i.CountInSet})" +
//                    $" - Цена({i.Price}) - Сломано({i.BlackMark}) - Почему сломано({i?.Comment}) - Списано({i.IsWriteOf})");
//            }
//        }

//        public void PrintTenants()
//        {
//            Console.WriteLine("АРЕНДАТОРЫ:");
//            foreach (var i in tenant_repository?.GetAllTenant().ToList<Tenant>())
//            {
//                Console.WriteLine($"ID({i.Id}) - Член клуба({i.IsClubman}) - Телефон({i.Phone})");
//            }
//            PrintLine();
//        }

//        public void PrintRentItens()
//        {
//            Console.WriteLine("ИСТОРИЯ ВЕЩЕЙ:");
//            rent_repository.GetRentById(6);
//            PrintLine();
//        }
        
//        public void PrintArticleRentHistory(int article_id)
//        {

//        }
//    }
//}
