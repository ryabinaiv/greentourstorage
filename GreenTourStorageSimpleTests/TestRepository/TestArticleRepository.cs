﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using GreenTourStorage.Repository;
//using GreenTourStorage.Models;

//namespace GreenTourStorageSimpleTests.TestRepository
//{
//    // Test Repository class for work with articles
//    public class TestArticleRepository : IArticleRepository
//    {
//        private IList<ParticularArticle> ParticularArticlesList;
//        private IList<MultipleArticle> MultipleArticlesList;
//        private IList<MultipleArticleSet> MultipleArticleSetsList;
//        private IList<Category> Categories;
//        private int ActualId;

//        public TestArticleRepository()
//        {
//            ParticularArticlesList = new List<ParticularArticle>();
//            MultipleArticlesList = new List<MultipleArticle>();
//            MultipleArticleSetsList = new List<MultipleArticleSet>();
//            Categories = new List<Category>();
//            ActualId = 1;
//        }

//        public ParticularArticle GetParticular(int id)
//        {
//            var pa = ParticularArticlesList.ToList<ParticularArticle>().Find(x => x.Id == id);
//            return pa;
//        }

//        public MultipleArticle GetMultiple(int id)
//        {
//            var ma = MultipleArticlesList.ToList<MultipleArticle>().Find(x => x.Id == id);
//            return ma;
//        }

//        public void InsertParticular(ParticularArticle pa)
//        {
//            ParticularArticlesList.Add(pa);
//        }

//        public void InsertMultiple(MultipleArticle ma)
//        {
//            MultipleArticlesList.Add(ma);
//        }

//        public List<Category> GetAllCategory()
//        {
//            return Categories.ToList<Category>();
//        }

//        public void InsertCategory(string category_name)
//        {
//            Categories.Add(new Category(GenericId(), category_name));
//        }

//        public int GenericId() => ActualId++;

//        public int GenericId(int? regnum)
//        {
//            if ((regnum <= 0) ||(!regnum.HasValue))
//                return GenericId(); 
//            var pa = ParticularArticlesList.ToList<ParticularArticle>().Find(x => x.Id == regnum);
//            var ma = MultipleArticlesList.ToList<MultipleArticle>().Find(x => x.Id == regnum);
//            if ((pa == null) && (ma == null))
//                return regnum.Value;
//            else return GenericId();
//        }

//        public void SaveChangesParticular(ParticularArticle pa)
//        {
//            var pa_index = ParticularArticlesList.ToList<ParticularArticle>().FindIndex(x => x.Id == pa.Id);
//            if (pa_index == -1) return;
//            ParticularArticlesList[pa_index] = pa;
//        }

//        public void SaveChangesMultiple(MultipleArticle ma)
//        {
//            var ma_index = MultipleArticlesList.ToList<MultipleArticle>().FindIndex(x => x.Id == ma.Id);
//            if (ma_index == -1) return;
//            MultipleArticlesList[ma_index] = ma;
//        }

//        public List<ParticularArticle> GetAllParticular() => ParticularArticlesList.ToList<ParticularArticle>();
//        public List<MultipleArticle> GetAllMultiple() => MultipleArticlesList.ToList<MultipleArticle>();
//    }

//}
