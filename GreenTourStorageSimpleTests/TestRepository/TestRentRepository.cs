﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using GreenTourStorage.Repository;
//using GreenTourStorage.Models;

//namespace GreenTourStorageSimpleTests.TestRepository
//{
//    public class TestRentRepository : IRentRepository
//    {
//        public class Item
//        {
//            public int RentId { get; }
//            public int ArticleId { get; }
//            public int SetId { get; }
//            public int Type { get; } // 1 or 2
//            public bool IsDelete { get; private set; }

//            public Item(int rent_id, int article_id, int set_id, int type, bool is_delete)
//            {
//                RentId = rent_id;
//                ArticleId = article_id;
//                Type = type;
//                SetId = set_id;
//                IsDelete = is_delete;
//            }
//            public void AddDeleteMark() { IsDelete = true; }
//        }

//        private int ActualId;
//        public IList<Rent> RentsList;
//        private IList<Item> RentItemsList;

//        public TestRentRepository()
//        {
//            ActualId = 1;
//            RentsList = new List<Rent>();
//            RentItemsList = new List<Item>();
//        }

//        private void PrintItems()
//        {
//            foreach (var i in RentItemsList)
//            {
//                Console.WriteLine($"Аренда({i.RentId}) - Вещь({i.ArticleId}) - Сет({i.SetId}) " +
//                    $"- Тип({i.Type}) - удалено({i.IsDelete})");
                
//            }Console.WriteLine("_____________________________________________________________________");
//        }

//        public int GenericId() => ActualId++;

//        public void InsertRent(Rent rent)
//        {
//            RentsList.Add(rent);
//            foreach (var item in rent.RentItems.GetParticularArticlesList())
//            {
//                if (item == null) break;
//                Item new_item = new Item(rent.RentId, item.Id, item.Id, 1, false);
//                RentItemsList.Add(new_item);
//            }
//            foreach (var item in rent.RentItems.GetMultipleArticleSetsList())
//            {
//                if (item == null) break;
//                Item new_item = new Item(rent.RentId, item.ArticleId, item.Id, 2, false);
//                RentItemsList.Add(new_item);
//            }
//        }

//        public void CanselRent(int rent_id)
//        {
//            var rent = RentsList.ToList<Rent>().Find(x => x.RentId == rent_id);
//            if (rent == null) return;
//            foreach (var item in RentItemsList)
//            {
//                if (item.RentId == rent_id)
//                {
//                    item.AddDeleteMark();
//                }
//            }
//        }

//        public List<Rent> GetArticleRentStory(int article_id)
//        {
//            var result_list = new List<Rent>();
//            var rent_id_list = new List<int>();
//            foreach (var i in RentItemsList)
//            {
//                if (i.ArticleId == article_id)
//                    rent_id_list.Add(i.RentId);
//            }
//            if (rent_id_list.Count == 0) return null;
//            foreach (var i in rent_id_list)
//            {
//                var buff = RentsList.ToList<Rent>().Find(x => x.RentId == i);
//                if (buff != null)
//                    result_list.Add(buff);
//            }
//            return result_list.Count == 0 ? null : result_list;
//        }

//        public Rent GetRentById(int id) { /*PrintItems();*/  return RentsList.ToList<Rent>().Find(x => x.RentId == id);  }

//        public List<Rent> GetAllRents() => RentsList.ToList<Rent>();
//    }
//}
