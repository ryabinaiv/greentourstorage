﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using GreenTourStorage.Repository;
//using GreenTourStorage.Models;

//namespace GreenTourStorageSimpleTests.TestRepository
//{
//    // Test class for Tenants Repository
//    public class TestTenantRepository : ITenantRepository
//    {
//        private List<Tenant> TenantsList;
//        private int ActualId;

//        public TestTenantRepository()
//        {
//            TenantsList = new List<Tenant>();
//            ActualId = 1;
//        }

//        public int GenericId() => ActualId++;

//        public void InsertTenant(Tenant tenant)
//        {
//            if (TenantsList.Contains(tenant)) return;
//            TenantsList.Add(tenant);
//        }


//        public IList<Tenant> GetAllTenant() => new List<Tenant>(TenantsList);

//        public Tenant GetTenantById(int id) => TenantsList.ToList<Tenant>().Find(x => x.Id == id);

//        public void SaveChanges(Tenant tenant)
//        {
//            int index = TenantsList.ToList<Tenant>().FindIndex(x => x.Id == tenant.Id);

//            if (index != -1)
//            {
//                TenantsList[index] = tenant;
//            }
//        }
//    }
//}
