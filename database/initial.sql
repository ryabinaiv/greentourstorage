--create database GreenTourStorageDB;

create table Categories
(
    CategoryId Int primary key Identity,
    Name nvarchar(50)  not null
);

create table Tenants
(
    Id Int primary key identity,
    Phone nvarchar(20),
    Nickname nvarchar(20),
    State int not null,
);

create table MultipleArticles
(
    Id Int primary key identity,
    RegNum Int Unique not null,
    Name nvarchar(50) not null,
    Category nvarchar(50) not null,
    Price Decimal(18, 2) not null,
    Description nvarchar(150),
    Weight Decimal(18, 2) null,

    TotalCount Int not null,
    ActualCount Int not null,
    InRepairCount Int not null
);

create table ParticularArticles
(
    Id Int primary key identity,
    RegNum Int Unique not null,
    Name nvarchar(50) not null,
    Category nvarchar(50) not null,
    Price Decimal(18,2) not null,
    Description nvarchar(150),
    Weight Decimal(18,2) null,
    Photo VARBINARY,
    RentId Int null,

    BlackMark Bit not null,
    Comment nvarchar(4000),
    TetantId Int References Tenants(Id),
    IsWriteOf Bit not null
);

create table Rents
(
    RentId Int primary key,
    TenantId Int references Tenants(Id) not null,
    IsFreeRent Bit not null,
    Comment nvarchar(150),
    IsFinished Bit not null,
    MadePayValue Decimal(18,2),
    RentBeginDate Date not null,
    RentEndDate Date null,
    RentCostInWeek Decimal(18,2) not null,
);

create table RentItems
(
    RentId Int references Rents(RentId) not null,
    ArticlesId Int not null,
    Price Decimal(18,2) not null,         
    Count Int not null,

    ArticleType Int,
    ToRepair Int not null,
    Returned Int not null,
    Comment nvarchar(4000)

    CONSTRAINT RentItems_PK PRIMARY KEY CLUSTERED (RentId, ArticlesId)
);